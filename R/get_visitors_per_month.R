#' Calculate and return visitors per month
#'
#' This function creates a table of the visitors registered by
#' goatcounter for the Wmo dashboards.
#'
#' Before you can run the code, the code must have access to the goatcounter.com
#' api. Therefore a key needs to be created, this can be done in your account
#' (User → API) at goatcounter.com.
#' See also https://www.goatcounter.com/help/api.
#'
#' Saving key in Renviron file
#'
#' The key can be saved in the .Renviron file as pointed out
#' [here](https://cran.r-project.org/web/packages/httr/vignettes/secrets.html)
#' The file must look like:
#'
#' `GOATCOUNTER_KEY=xxxx`
#'
#' where xxxx needs to be replaced by the key. The analysis and visualization
#' can subsequently be run as shown below.
#'
#' Adding key to function call
#'
#' Alternatively the key can be provided in the function call. In this case the
#' analysis and visualization can be run as shown in the examples.
#'
#'
#' @param title Provide input on which dashboards or websites to plot
#' @param ... further arguments to pass on to download_data()
#'
#' @return Data.Frame
#'
#' @importFrom magrittr %>%
#' @importFrom rlang .data
#'
#' @export
#'
#' @examples
#' \dontrun{
#' visitors_per_month()
#' }
#'
visitors_per_month <- function(title="Wmo", ...) {
  df.raw <- download_data(...)

  # filter bots out
  df <- df.raw %>% dplyr::filter(.data$Bot==0)

  # Datums toevoegen
  df <- df %>%
    dplyr::mutate(
      Date = as.POSIXct(.data$Date, format = "%Y-%m-%dT%H:%M:%SZ", tz="UTC"),
      datum = as.Date(.data$Date, format = "%Y-%m-%d" , tz="Europe/Amsterdam"),
      month = lubridate::month(.data$datum, label=TRUE)
    )


  # summarize per month
  df.month <- df %>%
    dplyr::filter(grepl(title, .data$Title)) %>%
    dplyr::group_by(.data$Title, .data$month) %>%
    dplyr::summarise(bezoekers=dplyr::n_distinct(.data$Session))

  # print average
  print(
    df.month %>%
      dplyr::group_by(.data$Title) %>%
      dplyr::summarise(mean(.data$bezoekers))
  )

  return(df.month)
}
