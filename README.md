
<!-- README.md is generated from README.Rmd. Please edit that file -->

# geitjesanalyse

<!-- badges: start -->
<!-- badges: end -->

The goal of geitjesanalyse is to provide tools to analyse visitor counts

## Installation

You can install the released version of geitjesanalyse from Gitlab with:

``` r
install.packages("devtools")
devtools::install_gitlab("https://gitlab.com/commondatafactory/datascience/geitjesanalyse")
```

## Running the code

Before you can run the code, the code must have access to the
goatcounter.com api. Therefore a key needs to be created, this can be
done in your account (User → API) at goatcounter.com. See also
<https://www.goatcounter.com/help/api>.

### Saving key in Renviron file

The key can be saved in the .Renviron file as pointed out
[here](https://cran.r-project.org/web/packages/httr/vignettes/secrets.html):

`file.edit("~/.Renviron")`

The file must look like:

`GOATCOUNTER_KEY=xxxx`

where xxxx needs to be replaced by the key. The analysis and
visualization can subsequently be run as:

``` r
library(geitjesanalyse)
## plotting visitors per day over time
visitors_per_day()
```

A table with montly visitor counts can be obtained by:

``` r
library(geitjesanalyse)
## getting table with visitors per month
df <- visitors_per_month()
#> [1] "trying..."
#> [1] "trying..."
#> # A tibble: 2 × 2
#>   Title                   `mean(.data$bezoekers)`
#>   <chr>                                     <dbl>
#> 1 Wmo gemeentevergelijker                    247.
#> 2 Wmo voorspelmodel                          460.
print(head(df))
#> # A tibble: 6 × 3
#> # Groups:   Title [1]
#>   Title                   month bezoekers
#>   <chr>                   <ord>     <int>
#> 1 Wmo gemeentevergelijker May         477
#> 2 Wmo gemeentevergelijker Jun         347
#> 3 Wmo gemeentevergelijker Jul         368
#> 4 Wmo gemeentevergelijker Aug         154
#> 5 Wmo gemeentevergelijker Sep         113
#> 6 Wmo gemeentevergelijker Oct         178
```

### Adding key to function call

Alternatively the key can be provided in the function call. In this case
the analysis and visualization can be run as:

``` r
library(geitjesanalyse)
## basic example code
visitors_per_day(key="xxxx")
```

where xxxx needs to be replaced by the key.

## Niene’s analysis

The function `Nienes_analysis(key="XXX")` downloads the visitor data for
the dashboards dego.vng.nl, dook.vng.nl, wijkpaspoort.nl, subsequently
analyses that data and creates figures. The key needs to be created,
this can be done in your account (User → API) at goatcounter.com. See
also <https://www.goatcounter.com/help/api>. This code can be run as:

``` r
library(geitjesanalyse)
# basic example code
Nienes_analysis(key="xxxx")
```

## Licentie

<a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.nl"><img alt="Creative Commons-Licentie" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />Dit
werk valt onder een
<a rel="license" href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.nl">Creative
Commons Naamsvermelding-NietCommercieel-GelijkDelen 4.0
Internationaal-licentie</a>.
